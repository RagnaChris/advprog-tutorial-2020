package id.ac.ui.cs.advprog.tutorial2.template.core;

import java.util.ArrayList;
import java.util.List;

public abstract class SpiritInQuest {

    public List attackPattern() {
        List list = new ArrayList();
        // TODO: Complete Me
        list.add(summon());
        list.add(getReady());
        list.add(buff());
        List list2 = new ArrayList();
        list2.add(attackWithBuster());
        list2.add(attackWithQuick());
        list2.add(attackWithArts());
        if (this.getClass().getName() == "Archer"){
            list.add(attackWithBuster());
        }
        else if (this.getClass().getName() == "Lancer"){
            list.add(attackWithArts());
        }
        else {
            list.add(attackWithQuick());
        }

        for (int i = 0; i < list2.size(); i++){
            if (!list.contains(list2.get(i))){
                list.add(list2.get(i));
            }
        }
        list.add(attackWithSpecialSkill());
        return list;
    }


    public String summon() {
        return "Summon a Spirit...";
    }

    public String getReady() {
        return "Spirit ready to enter the Quest";
    }

    protected abstract String buff();

    protected abstract String attackWithBuster();

    protected abstract String attackWithQuick();

    protected abstract String attackWithArts();

    protected abstract String attackWithSpecialSkill();
}
