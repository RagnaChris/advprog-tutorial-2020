package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;
    private boolean undoAllow = true;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        this.spells.get(spellName).cast();
        this.latestSpell = this.spells.get(spellName);
        this.undoAllow = true;
    }

    public void undoSpell() {
        // TODO: Complete Me
        if (this.latestSpell == null){
            return;
        }
        if (this.undoAllow == true) {
            this.latestSpell.undo();
            this.undoAllow = false;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
